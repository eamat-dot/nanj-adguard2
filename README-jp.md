# なんJ AdGuard部 モバイル用フィルターリスト @私家版

[なんJAdGuard部モバイル用フィルターリスト](https://wikiwiki.jp/nanj-adguard/%E3%83%A2%E3%83%90%E3%82%A4%E3%83%AB%E7%94%A8%E3%83%95%E3%82%A3%E3%83%AB%E3%82%BF%E3%83%BC%E3%83%AA%E3%82%B9%E3%83%88#m529d4e2)より

ユーザールールのコピペが面倒なのでカスタムフィルタ用txtファイルを作成  
AdGuard プレミアム版用

## コンテンツブロック

~~AdGuard設定 - コンテンツブロック - ユーザールール - インポート~~【無料版用設定】  

AdGuard設定 - コンテンツブロック - フィルタ - カスタムフィルタ【プレミアム版用設定】  
＋ 新規カスタムフィルタにてURLを指定

* 280blocker adblock filter  
~~`https://280blocker.net/files/280blocker_adblock_[yyyymm].txt`~~  
自動更新版 `https://fresh-fern-lantana.glitch.me/280blocker_adblock.txt`

* 280blocker AdblockPlus形式 悪質サイト対策強化パッチ  
`https://raw.githubusercontent.com/Yuki2718/adblock/master/japanese/280-patch.txt`

* なんJ改修フィルター（テキストファイル版）  
`https://gitlab.com/eamat-dot/nanj-adguard2/-/raw/master/nanj-kaishuu-filter/nanj-kaishuu-filter.txt`
* なんJ拡張フィルター：一般ルール（テキストファイル版）  
`https://gitlab.com/eamat-dot/nanj-adguard2/-/raw/master/nanj-kakutyou-filter/supplement-rules.txt`
* なんJ拡張フィルター：Paranoidルール（テキストファイル版）  
`https://gitlab.com/eamat-dot/nanj-adguard2/-/raw/master/nanj-kakutyou-filter/paranoid-rules.txt`


## DNSフィルタリング用のフィルタ

AdGuard設定 - DNSフィルタリング - DNSフィルタをカスタマイズ  
＋ DNSフィルタを追加

* ~~280blocker domain filter（AdGuard_format）~~ 【無料版用設定】  
~~`https://280blocker.net/files/280blocker_domain_ag_[yyyymm].txt`~~

* 280blocker adblock filter 【プレミアム版用設定】   
~~`https://280blocker.net/files/280blocker_adblock_[yyyymm].txt`~~  
自動更新版 `https://fresh-fern-lantana.glitch.me/280blocker_adblock.txt`

* なんJ改修DNSフィルター（テキストファイル版）  
`https://gitlab.com/eamat-dot/nanj-adguard2/-/raw/master/nanj-kaishuu-filter/nanj-kaishuu-dns-filter.txt`

* なんJ拡張フィルター：DNSルール（テキストファイル版）  
`https://gitlab.com/eamat-dot/nanj-adguard2/-/raw/master/nanj-kakutyou-filter/DNS-rules.txt`

